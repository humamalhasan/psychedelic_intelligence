 def visstd(a, s=0.1):
       
        return (a-a.mean())/max(a.std(), 1e-4)*s + 0.5
    
    def T(layer):
       
        return graph.get_tensor_by_name("import/%s:0"%layer)
    
    def render_naive(t_obj, img0=img_noise, iter_n=20, step=1.0):
        t_score = tf.reduce_mean(t_obj) 
        t_grad = tf.gradients(t_score, t_input)[0] 
        
        img = img0.copy()
        for _ in range(iter_n):
            g, _ = sess.run([t_grad, t_score], {t_input:img})   
            
            g /= g.std()+1e-8  
            img += g*step
        showarray(visstd(img))
        
    def tffunc(*argtypes):
        
        placeholders = list(map(tf.placeholder, argtypes))
        def wrap(f):
            out = f(*placeholders)
            def wrapper(*args, **kw):
                return out.eval(dict(zip(placeholders, args)), session=kw.get('session'))
            return wrapper
        return wrap
    
    def resize(img, size):
        img = tf.expand_dims(img, 0)
        return tf.image.resize_bilinear(img, size)[0,:,:,:]
    resize = tffunc(np.float32, np.int32)(resize)