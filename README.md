# Psychedelic_Intelligence

[![license](https://img.shields.io/github/license/mashape/apistatus.svg)]()
[![5](https://img.shields.io/github/languages/code-size/badges/shields.svg)]()
_____
Psychedelic_Intelligence it's about combining the psychedelic drugs experiences with every object around you so all you have to give for the core part of the algorithm is how much Psychedelic Drug you want to take ex(lsd,mushrooms,madma).


#### Used Libs.

  - numpy
  - tensorflow
  - os
  - zipfile
  - matplotlib.pyplot
  - Image
  - urllib.request

### Results

# While training the nets 
_____
[![1](https://github.com/hgobv/Psychedelic_Intelligence/blob/master/results/final_f.png?raw=true)]()
_____
#### 1
___
[![1](https://github.com/hgobv/Psychedelic_Intelligence/blob/master/results/beforeafter/1.jpg?raw=true)]()

_____
#### 2
____
[![1](https://github.com/hgobv/Psychedelic_Intelligence/blob/master/results/beforeafter/6.JPG?raw=true
)]()

____
#### 3
___
[![1](https://github.com/hgobv/Psychedelic_Intelligence/blob/master/results/beforeafter/3.jpg?raw=true)]()

_____
#### 4
___
[![1](https://github.com/hgobv/Psychedelic_Intelligence/blob/master/results/beforeafter/4.jpg?raw=true)]()

_____
#### 5
___
[![1](https://github.com/hgobv/Psychedelic_Intelligence/blob/master/results/beforeafter/2.jpg?raw=true)]()



### Installation

Psychedelic_Intelligence requires [python3](https://python.org/) to run.



```sh
$ git clone https://github.com/hgobv/Psychedelic_Intelligence
$ \\ combining algorithm files 🤡
$ python trip.py
```






License
----

MIT


**Free Software, Hell Yeah!**
Soon For Cloud. ❤️

